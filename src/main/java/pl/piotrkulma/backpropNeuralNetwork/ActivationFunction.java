package pl.piotrkulma.backpropNeuralNetwork;

public class ActivationFunction {
    public static float sigmoid(float x) {
        return (float) (1 / (1 + Math.exp(-x)));
    }

    //sigmoid'
    //x must be sigmoid(x)
    public static float dSigmoid(float x) {
        return x * (1 - x);
    }
}
