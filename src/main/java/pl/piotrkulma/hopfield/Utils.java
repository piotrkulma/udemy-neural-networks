package pl.piotrkulma.hopfield;

import java.util.Arrays;

public class Utils {
    public static double toBipolar(double p) {
        if (p == 0) {
            return -1;
        }

        return 1;
    }

    public static double[] transform(double[] pattern) {
        return Arrays.stream(pattern)
                .map(Utils::toBipolar)
                .toArray();
    }
}
